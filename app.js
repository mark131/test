const express = require('express')
const helmet = require('helmet')
const morgan = require('morgan')
const cors = require("cors")

require('dotenv').config()
require('./db');

const app = express()
const port = process.env.Port || 4000

app.use(helmet())
app.use(cors())
app.use(express.json())
app.use(morgan('dev'))


//routes
app.use('/api/users', require('./routes/User'))


app.listen(port,()=>{
    console.log(`Server runnning on port ${port}`)
})