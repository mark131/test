const mongoose = require('mongoose')

const transactionSchema = new mongoose.Schema({
 user : {
     type : mongoose.Schema.Types.ObjectId,
     ref : 'User',

 },
 total: {
     type: Number,
     
 }
})

module.exports = mongoose.model('transaction', transactionSchema)